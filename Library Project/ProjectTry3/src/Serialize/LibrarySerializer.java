package Serialize;

import project.ILibrary;
import project.Library;

import java.io.*;

public class LibrarySerializer implements Serializable {

    public void serialize(ILibrary library, String path) {
        try {

            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(library);
            out.close();
            fileOut.close();

        } catch (IOException i) {

            i.printStackTrace();

        }
    }

    public ILibrary deserialize(String path) {
        ILibrary library = null;

        try {

            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            library = (Library) in.readObject();
            in.close();
            fileIn.close();

        } catch (IOException | ClassNotFoundException i) {

            i.printStackTrace();

        }

        return library;
    }

}