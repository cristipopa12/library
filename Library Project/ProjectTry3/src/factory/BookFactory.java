package factory;

import project.Author;
import project.Book;
import project.IBook;

import java.util.ArrayList;
import java.util.List;

public class BookFactory implements IBookFactory {
    @Override
    public IBook createBook(String name, String isbn, String authorName, int otherNumberOfCopies, String publisher) {

        IAuthorFactory authorFactory = new AuthorFactory();

        Author author = authorFactory.createAuthor(authorName);

        List<Author> authorList = new ArrayList<>();

        authorList.add(author);

        return new Book(name, isbn, authorList, otherNumberOfCopies, publisher);
    }
}
