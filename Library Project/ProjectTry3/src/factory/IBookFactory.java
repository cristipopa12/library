package factory;

import project.Book;
import project.IBook;

public interface IBookFactory {
    IBook createBook(String name, String isbn, String author, int numberOfCopies, String publisher);
}
