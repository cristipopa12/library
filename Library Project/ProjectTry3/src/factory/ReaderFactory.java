package factory;

import project.IReader;
import project.Reader;

public class ReaderFactory implements IReaderFactory {
    @Override
    public IReader createReader(String name, int cnp) {
        return new Reader(name,cnp);
    }
}
