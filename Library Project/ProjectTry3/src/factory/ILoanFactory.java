package factory;

import project.IBook;
import project.ILoan;
import project.IReader;

public interface ILoanFactory {
    ILoan createLoan(IBook book, IReader reader);
}
