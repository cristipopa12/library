package factory;

import project.IReader;
import project.Reader;

public interface IReaderFactory {
    IReader createReader(String name, int cnp);

}
