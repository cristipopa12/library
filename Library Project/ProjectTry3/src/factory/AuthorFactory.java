package factory;

import project.Author;

public class AuthorFactory implements IAuthorFactory {

    @Override
    public Author createAuthor(String authorName) {
        return new Author(authorName);
    }
}
