package factory;

import project.IBook;
import project.ILoan;
import project.IReader;
import project.Loan;

import java.time.LocalDate;
import java.util.Date;

public class LoanFactory implements ILoanFactory {

    @Override
    public ILoan createLoan(IBook book, IReader reader) {
        return new Loan(reader, book, LocalDate.now());
    }
}
