package factory;

import project.Author;

public interface IAuthorFactory {
    Author createAuthor(String authorName);
}
