package project;

import factory.AuthorFactory;
import factory.IAuthorFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Book implements IBook, Serializable {

    private String name;
    private String isbn;
    private List<Author> authorsOfTheBook;
    private int numberOfCopies;
    private String publisher;

    public Book(String name, String isbn, List<Author> authorsOfTheBook, int numberOfCopies, String publisher) {
        this.name = name;
        this.isbn = isbn;
        this.authorsOfTheBook = authorsOfTheBook;
        this.numberOfCopies = numberOfCopies;
        this.publisher = publisher;
    }

    @Override
    public List<String> getAuthors() {

        List<String > authorsNames = new ArrayList<>();

        for (Author iterator:authorsOfTheBook) {
            authorsNames.add(iterator.name);
        }

        return authorsNames;
    }

    @Override
    public void addNewCopy() {
        numberOfCopies ++;
    }

    @Override
    public void removeCopy() {
        numberOfCopies --;
    }

    @Override
    public void addAuthor(String author) {
        IAuthorFactory authorFactory = new AuthorFactory();

        Author authorName = authorFactory.createAuthor(author);

        authorsOfTheBook.add(authorName);
    }

    @Override
    public String getPublisher() {
        return publisher;
    }

    @Override
    public String getTitle() {
        return name;
    }

    @Override
    public String getIsbn() {
        return isbn;
    }

    @Override
    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public String toString() {
        return  "Title: " + getTitle() + " ISBN: " + getIsbn() + " Authors: " + getAuthors() + " Nr. of copies: " + getNumberOfCopies() + " Publisher: " + getPublisher();
    }

}
