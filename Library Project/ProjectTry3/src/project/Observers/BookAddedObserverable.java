package project.Observers;

import java.util.Observable;

public class BookAddedObserverable extends Observable {
    public void changeMessage(String message)
    {
        setChanged();
        notifyObservers(message);
    }
}
