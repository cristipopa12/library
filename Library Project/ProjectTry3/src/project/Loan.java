package project;

import java.io.Serializable;
import java.time.*;
import java.time.temporal.ChronoUnit;

public class Loan implements ILoan, Serializable {

    public static final int LOAN_PERIOD = 10;

    IReader reader;
    IBook book;
    LocalDate returnDay;

    public Loan(IReader reader, IBook book, LocalDate returnDay) {
        this.returnDay = LocalDate.now().plus(LOAN_PERIOD, ChronoUnit.DAYS);
        this.reader = reader;
        this.book = book;
    }

    @Override
    public void extendPeriod(int extendedPeriod) {
        returnDay = returnDay.plus(extendedPeriod, ChronoUnit.DAYS);
    }

    @Override
    public IReader getReader() {
        return reader;
    }

    @Override
    public IBook getBook() {
        return book;
    }

    @Override
    public LocalDate getDate() {
        return returnDay;
    }

    public String toString(){
        return "Reader: " + getReader() + " Book: " + getBook() + " ReturnDay: " + getDate();
    }

}
