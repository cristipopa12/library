package project;

import Serialize.LibrarySerializer;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Library implements ILibrary, Serializable {

    private List<IBook> booksList = new ArrayList<>();
    private List<ILoan> loanList = new ArrayList<>();
    private Authors authors;

    public Library(String path) {

        File serFile = new File(path);
        if(serFile.exists() && !serFile.isDirectory()) {
            this.booksList = new LibrarySerializer().deserialize(path).getBooksList();
             this.loanList = new LibrarySerializer().deserialize(path).getLoanList();
        }

    }

    @Override
    public void addBook(IBook book) {
        booksList.add(book);
    }

    @Override
    public void removeBook(IBook book) {
        booksList.remove(book);
    }

    @Override
    public void makeNewLoan(ILoan loan) {

        loanList.add(loan);
    }

    @Override
    public void returnLoan(ILoan loan) {
        loanList.remove(loan);
    }

    @Override
    public List<IBook> searchBooksByAuthor(String author) {
        return authors.getBooksByAuthor(author);
    }

    @Override
    public List<IBook> searchBooksByAuthor(Author author) {
        return searchBooksByAuthor(author.name);
    }

    @Override
    public List<IBook> searchBooksByTitle(String title) {
        return null;
    }

    @Override
    public List<IBook> getBooks() {
        return booksList;
    }

    public List<IBook> getBooksList() {
        return booksList;
    }

    public void setBooksList(List<IBook> booksList) {
        this.booksList = booksList;
    }

    public List<ILoan> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<ILoan> loanList) {
        this.loanList = loanList;
    }
}
