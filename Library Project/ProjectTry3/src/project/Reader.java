package project;

import java.io.Serializable;

public class Reader implements IReader, Serializable {
    String name;
    int cnp;

    public Reader(String name, int cnp) {
        this.name = name;
        this.cnp = cnp;
    }

    public String toString(){
        return "Nume: " + name + " CNP: " + cnp;
    }
}
