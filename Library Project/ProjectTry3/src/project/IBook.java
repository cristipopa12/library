package project;

import java.util.List;

public interface IBook {

    List<String> getAuthors();

    void addNewCopy();

    void removeCopy();

    void addAuthor(String author);

    String getPublisher();

    String getTitle();

    String getIsbn();

    int getNumberOfCopies();
}
