package project;

import java.time.LocalDate;

public interface ILoan {
    void extendPeriod(int extendedPeriod);

    IReader getReader();

    IBook getBook();

    LocalDate getDate();
}
