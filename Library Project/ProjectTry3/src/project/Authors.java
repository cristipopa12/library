package project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Authors implements Serializable {

    private Map<String, List<IBook>> booksByAuthor;

    public Authors()
    {
        booksByAuthor = new HashMap<String, List<IBook>>();
    }

    public void add(IBook book)
    {
        for (String author:book.getAuthors()) {
            addBookToAuthor(author,book);
        }
    }

    public List<IBook> getBooksByAuthor(String author){
        return booksByAuthor.getOrDefault(author,new ArrayList<IBook>());
    }

    private void addBookToAuthor(String author, IBook book){
        List<IBook> books = booksByAuthor.getOrDefault(author, new ArrayList<IBook>());
        if (books.isEmpty()) {
            books.add(book);
            booksByAuthor.put(author, books);
        } else {
            books.add(book);
        }
    }
}
