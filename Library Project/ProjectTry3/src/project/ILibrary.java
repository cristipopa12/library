package project;

import java.util.List;

public interface ILibrary {
    void addBook(IBook book);

    void removeBook(IBook book);

    void makeNewLoan(ILoan loan);

    void returnLoan(ILoan loan);

    List<IBook> searchBooksByAuthor(String author);

    List<IBook> searchBooksByAuthor(Author author);

    List<IBook> searchBooksByTitle (String title);

    List<IBook> getBooks();

    List<IBook> getBooksList();
    List<ILoan> getLoanList();

}
