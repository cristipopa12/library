package project;

import UI.LibraryController;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Application {

    LibraryController libController;

    public static void main(String[] args) {
        Application application = new Application();
        application.launch();
    }

    private void launch() {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    libController = new LibraryController();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void init() {
        File f = new File("./repository.ser");
        if(f.exists() && !f.isDirectory()) {
            try {
                FileInputStream fileIn = new FileInputStream("./repository.ser");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                libController = (LibraryController) in.readObject();
                in.close();
                fileIn.close();

            } catch (IOException | ClassNotFoundException i ) {

                libController = new LibraryController();
                i.printStackTrace();

            }
        }else {
            libController = new LibraryController();
        }
    }
}
