package UI;
import javax.swing.*;
import java.io.Serializable;

public class LibraryView extends JFrame  implements Serializable {

    JTabbedPane tabbedPane;

    public LibraryView() {
        setBounds(100, 100, 1000, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        initialize();
    }

    private void initialize() {

        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(10, 11, 964, 539);
        getContentPane().add(tabbedPane);

        setResizable(false);

    }

    public void addTab(JPanel jPanel, String title)
    {
        tabbedPane.addTab(title, null, jPanel, null);
        jPanel.setLayout(null);
    }
}

