package UI;

import project.ILibrary;
import project.Library;

import java.io.Serializable;

public class LibraryController implements Serializable {

    private ILibrary library;
    //private LibrarySerializer librarySerializer;
    private BookEditorController bookEditorController;
    private LoanEditorController loanEditorController;
    private SearchEditorController searchEditorTabView;

    public LibraryController() {

        library = new Library("library.ser");
        LibraryView libraryView = new LibraryView();
        libraryView.setVisible(true);

        bookEditorController = new BookEditorController(library, libraryView);
        loanEditorController = new LoanEditorController(library, libraryView);
        searchEditorTabView = new SearchEditorController(library, libraryView);

        //librarySerializer = new LibrarySerializer("C:\\Users\\Cristi\\Desktop\\ProjectTry3\\ProjectTry3\\file.txt");

    }
}
