package UI;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class BookEditorTabView extends JPanel implements Serializable {

    private JTextField isbnTextField;
    private JTextField title;
    private JTextField authorTextField;
    private JTextField publisherTextField;
    private JTextField newAuthorTextField;

    public JButton btnAddBook;
    public JList bookJlist;
    public JButton btnRemoveBook;
    public JButton btnAddAuthor ;
    public JScrollPane bookListScrollPane;
    public DefaultListModel bookListModel;
    public JTextField copies;
    public JLabel lblX;

    public BookEditorTabView()
    {
        init();
    }

    private void init() {
        setLayout(null);

        JLabel isbnLabel = new JLabel("ISBN");
        isbnLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
        isbnLabel.setBounds(10, 34, 93, 25);
        add(isbnLabel);

        isbnTextField = new JTextField();
        isbnTextField.setBounds(113, 36, 164, 25);
        add(isbnTextField);
        isbnTextField.setColumns(10);

        JLabel lblTitle = new JLabel("Title");
        lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblTitle.setBounds(10, 70, 93, 25);
        add(lblTitle);

        title = new JTextField();
        title.setBounds(113, 70, 164, 25);
        add(title);
        title.setColumns(10);

        JLabel lblAuthor = new JLabel("Author");
        lblAuthor.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblAuthor.setBounds(10, 106, 93, 25);
        add(lblAuthor);

        authorTextField = new JTextField();
        authorTextField.setBounds(113, 106, 164, 25);
        add(authorTextField);
        authorTextField.setColumns(10);

        JLabel lblNrOfCopies = new JLabel("Nr. of copies");
        lblNrOfCopies.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblNrOfCopies.setBounds(10, 142, 93, 25);
        add(lblNrOfCopies);

        copies = new JTextField();
        copies.setBounds(113, 142, 164, 25);
        add(copies);
        copies.setColumns(10);

        JLabel lblPublisher = new JLabel("Publisher");
        lblPublisher.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblPublisher.setBounds(10, 178, 93, 25);
        add(lblPublisher);

        publisherTextField = new JTextField();
        publisherTextField.setBounds(113, 178, 164, 25);
        add(publisherTextField);
        publisherTextField.setColumns(10);

        bookJlist = new JList();
        bookJlist.setBounds(356, 34, 552, 328);
        add(bookJlist);

        lblX = new JLabel("");
        lblX.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblX.setBounds(287, 142, 49, 25);
        add(lblX);

        bookListModel = new DefaultListModel();
        bookJlist.setModel(bookListModel);

        btnAddBook = new JButton("Add book");
        btnAddBook.setBounds(113, 215, 164, 42);

        add(btnAddBook);

        JPanel panel_1 = new JPanel();
        panel_1.setLayout(null);

        //frame.setUndecorated(true);

        btnRemoveBook = new JButton("Remove book");
        btnRemoveBook.setBounds(551, 373, 164, 42);
        add(btnRemoveBook);

        btnAddAuthor = new JButton("Add author");
        btnAddAuthor.setBounds(113, 373, 164, 42);
        add(btnAddAuthor);

        newAuthorTextField = new JTextField();
        newAuthorTextField.setColumns(10);
        newAuthorTextField.setBounds(113, 337, 164, 25);
        add(newAuthorTextField);

        bookListScrollPane = new JScrollPane();
        bookListScrollPane.setBounds(356, 34, 552, 328);
        add(bookListScrollPane);

        bookListScrollPane.setViewportView(bookJlist);
    }

    public String getAuthor()
    {
        return authorTextField.getText();
    }

    public String getTitle()
    {
        return title.getText();
    }

    public String getISBN()
    {
        return isbnTextField.getText();
    }

    public int getQuantity()
    {
        return Integer.valueOf(copies.getText());
    }

    public String getPublisher()
    {
        return publisherTextField.getText();
    }

    public String getNewAuthor() { return newAuthorTextField.getText(); }

    public void clearFields(){
        isbnTextField.setText(null);
        authorTextField.setText(null);
        title.setText(null);
        copies.setText(null);
        publisherTextField.setText(null);
        newAuthorTextField.setText(null);
    }
}
