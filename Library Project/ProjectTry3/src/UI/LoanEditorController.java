package UI;

import Serialize.LibrarySerializer;
import factory.ILoanFactory;
import factory.IReaderFactory;
import factory.LoanFactory;
import factory.ReaderFactory;
import project.IBook;
import project.ILibrary;
import project.ILoan;
import project.IReader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import static project.Loan.LOAN_PERIOD;

public class LoanEditorController implements Serializable {

    private ILibrary library;
    private LoanEditorTabView view;

    public LoanEditorController(ILibrary library, LibraryView libraryView) {
        this.library = library;
        view = new LoanEditorTabView();
        libraryView.addTab(view, "Loan editor");
        useActions();
    }

    private void useActions() {
        view.btnCreateLoan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] bookAtIndex = view.bookJList.getSelectedIndices();

                IReaderFactory readerFactory = new ReaderFactory();
                IReader reader = readerFactory.createReader(view.getReaderName(),view.getCnp());

                for (int iterator = 0; iterator < bookAtIndex.length; iterator++) {
                    if(bookAtIndex[iterator] < 0)
                        return;
                    else {
                        IBook bookToBeLoaned = (IBook) view.bookJList.getModel().getElementAt(bookAtIndex[iterator]);

                        ILoanFactory loanFactory = new LoanFactory();
                        ILoan newLoan = loanFactory.createLoan(bookToBeLoaned, reader);
                        library.makeNewLoan(newLoan);
                        bookToBeLoaned.removeCopy();
                        new LibrarySerializer().serialize(library, "library.ser");
                        view.loanListModel.addElement(newLoan);
                        view.clearFields();
                    }
                }

                view.bookListModel.removeAllElements();

                for (IBook iterator: library.getBooks()) {
                    if(iterator.getNumberOfCopies() > 0)
                        view.bookListModel.addElement(iterator);
                }
            }
        });

        view.btnUpdateBook.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.bookListModel.removeAllElements();


                for (IBook iterator: library.getBooks()) {
                    if(iterator.getNumberOfCopies() > 0)
                        view.bookListModel.addElement(iterator);
                }
                view.loanListModel.removeAllElements();
                for (ILoan iterator1: library.getLoanList()) {
                    view.loanListModel.addElement(iterator1);
                }
            }
        });

        view.btnExtendLoan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int loanAtIndex = view.loanJList.getSelectedIndex();
                if(loanAtIndex < 0)
                    return;

                ILoan loanToBeExtended = (ILoan) view.loanJList.getModel().getElementAt(loanAtIndex);
                loanToBeExtended.extendPeriod(LOAN_PERIOD);
                view.loanListModel.addElement(loanToBeExtended);
                view.loanListModel.removeElementAt(loanAtIndex);
            }
        });

        view.btnReturnLoan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int loanAtIndex =  view.loanJList.getSelectedIndex();
                if(loanAtIndex < 0)
                    return;

                ILoan returnedLoan = (ILoan) view.loanJList.getModel().getElementAt(loanAtIndex);

                view.loanListModel.remove(loanAtIndex);
                library.returnLoan(returnedLoan);
                returnedLoan.getBook().addNewCopy();

                view.bookListModel.removeAllElements();

                for (IBook iterator: library.getBooks()) {
                    if(iterator.getNumberOfCopies() > 0)
                        view.bookListModel.addElement(iterator);
                }
            }
        });
    }

}
