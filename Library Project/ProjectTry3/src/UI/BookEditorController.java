package UI;

import Serialize.LibrarySerializer;
import factory.BookFactory;
import factory.IBookFactory;
import project.IBook;
import project.ILibrary;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;

public class BookEditorController implements Serializable {

    private ILibrary library;

    BookEditorTabView view;
    private LibraryController libraryController;

    public BookEditorController(ILibrary library, LibraryView libraryView) {

        this.library = library;
        view = new BookEditorTabView();
        libraryView.addTab(view, "Book Editor");
        for (IBook bookIterator:library.getBooks()) {
            view.bookListModel.addElement(bookIterator);
        }
        useActions();
    }

    private void useActions(){
        view.btnAddBook.addActionListener(e -> {
            try{
                IBookFactory bookFactory = new BookFactory();
                IBook newBook = bookFactory.createBook(
                        view.getTitle(),
                        view.getISBN(),
                        view.getAuthor(),
                        view.getQuantity(),
                        view.getPublisher());

                view.bookListModel.addElement(newBook);

                library.addBook(newBook);

                new LibrarySerializer().serialize(library, "library.ser");
                view.clearFields();
                view.lblX.setText("");
            }
            catch (Exception e1){
                view.lblX.setText("X");
            }

        });


        view.bookJlist.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList) evt.getSource();
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    IBook value = (IBook) list.getModel().getElementAt(list.locationToIndex(evt.getPoint()));
                }
            }
        });


        view.btnRemoveBook.addActionListener(e -> {
            int bookAtIndex = view.bookJlist.getSelectedIndex();
            if (bookAtIndex < 0) return;
            IBook removedBook = (IBook) view.bookJlist.getModel().getElementAt(bookAtIndex);
            view.bookListModel.remove(bookAtIndex);
            library.removeBook(removedBook);

            view.bookJlist.revalidate();
            view.bookJlist.repaint();
        });

        view.btnAddAuthor.addActionListener(e -> {
            int bookAtIndex = view.bookJlist.getSelectedIndex();
            if(bookAtIndex < 0)
                return;
            IBook modifiedBook = (IBook) view.bookJlist.getModel().getElementAt(bookAtIndex);
            modifiedBook.addAuthor(view.getNewAuthor());
            view.bookListModel.addElement(modifiedBook);
            view.bookListModel.remove(bookAtIndex);
            view.clearFields();
        });
    }

}
