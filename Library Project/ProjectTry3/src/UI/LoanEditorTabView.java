package UI;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class LoanEditorTabView extends JPanel implements Serializable {

    public  JTextField cnpTextField;
    private  JTextField nameTextField;

    public JScrollPane bookScrollPane;
    public JScrollPane loanScrollPane;
    public JList bookJList;
    public JList loanJList;

    public DefaultListModel bookListModel;
    public DefaultListModel loanListModel;
    public JButton btnCreateLoan;
    public JButton btnExtendLoan;
    public JButton btnReturnLoan;
    public JButton btnUpdateBook;
    public JLabel labelX;

    public LoanEditorTabView() {

        setLayout(null);

        bookScrollPane = new JScrollPane();
        bookScrollPane.setBounds(20, 11, 500, 220);
        add(bookScrollPane);

        bookJList = new JList();
        bookScrollPane.setViewportView(bookJList);

        loanScrollPane = new JScrollPane();
        loanScrollPane.setBounds(20, 280, 500, 220);
        add(loanScrollPane);

        loanJList = new JList();
        loanScrollPane.setViewportView(loanJList);

        JLabel nameLabel = new JLabel("Name");
        nameLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
        nameLabel.setBounds(581, 13, 93, 25);
        add(nameLabel);

        JLabel lblCnp = new JLabel("CNP");
        lblCnp.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblCnp.setBounds(581, 49, 93, 25);
        add(lblCnp);

        labelX = new JLabel("");
        labelX.setFont(new Font("Tahoma", Font.PLAIN, 18));
        labelX.setBounds(823, 49, 49, 25);
        add(labelX);

        nameTextField = new JTextField();
        nameTextField.setColumns(10);
        nameTextField.setBounds(649, 15, 164, 25);
        add(nameTextField);

        cnpTextField = new JTextField();
        cnpTextField.setColumns(10);
        cnpTextField.setBounds(649, 49, 164, 25);
        add(cnpTextField);

        btnCreateLoan = new JButton("Create loan");
        btnCreateLoan.setBounds(649, 85, 164, 42);
        add(btnCreateLoan);

        btnExtendLoan = new JButton("Extend loan");
        btnExtendLoan.setBounds(649, 280, 164, 42);
        add(btnExtendLoan);

        btnReturnLoan = new JButton("Return loan");
        btnReturnLoan.setBounds(649, 362, 164, 42);
        add(btnReturnLoan);

        btnUpdateBook = new JButton("Update");
        btnUpdateBook.setBounds(649, 189, 164, 42);
        add(btnUpdateBook);

        bookListModel = new DefaultListModel();
        bookJList.setModel(bookListModel);

        loanListModel = new DefaultListModel();
        loanJList.setModel(loanListModel);
    }

    public String getReaderName(){
        return nameTextField.getText();
    }

    public int getCnp(){
        return Integer.valueOf(cnpTextField.getText());
    }

    public void clearFields(){
        cnpTextField.setText(null);
        nameTextField.setText(null);
    }

}
