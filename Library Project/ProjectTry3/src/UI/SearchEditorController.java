package UI;

import project.ILibrary;

import java.io.Serializable;

public class SearchEditorController implements Serializable {

    private ILibrary library;
    private SearchEditorTabView view;

    public SearchEditorController(ILibrary library, LibraryView libraryView) {
        this.library = library;
        view = new SearchEditorTabView();

        libraryView.addTab(view, "Search books");
    }
}
